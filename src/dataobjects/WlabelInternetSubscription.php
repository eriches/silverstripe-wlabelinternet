<?php

namespace Hestec\WlabelInternet;

use SilverStripe\ORM\DataObject;

class WlabelInternetSubscription extends DataObject {

    private static $singular_name = 'WlabelInternetSubscription';
    private static $plural_name = 'WlabelInternetSubscriptions';

    private static $table_name = 'WlabelInternetSubscription';

    private static $db = array(
        'BaseId' => 'Int',
        'BaseName' => 'Varchar(255)',
        'Category' => 'Varchar(50)',
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)',
        'Provider' => 'Varchar(50)',
        'ProviderId' => 'Int',
        'ProviderName' => 'Varchar(50)',
        'BasePrice' => 'Currency',
        'BasePriceInitial' => 'Currency',
        'DiscountPrice' => 'Currency',
        'DiscountAmount' => 'Currency',
        'DiscountAmountInitial' => 'Currency',
        'DiscountAmountMonthly' => 'Currency',
        'DiscountPriceInitial' => 'Currency',
        'DiscountDuration' => 'Int',
        'DiscountType' => 'Varchar(20)',
        'TotalPriceInitial' => 'Currency',
        'TotalPrice' => 'Currency',
        'TotalPriceDiscounted' => 'Currency',
        'ComparePrice' => 'Currency',
        'ContractDuration' => 'Varchar(20)',
        'DownloadSpeed' => 'Int',
        'ConnectionType' => 'Varchar(50)',
        'UploadSpeed' => 'Int',
        'TvChannelsTotal' => 'Int',
        'TvChannelsHd' => 'Int',
        'DiscountText' => 'Text',
        'OfferText' => 'Text',
        'ExtraIncluded' => 'MultiValueField',
        'ExtraOptional' => 'MultiValueField',
        'Score' => 'MultiValueField',
        'RatingVotes' => 'Int',
        'RatingScore' => 'Int',
        'SignupUrl' => 'Varchar(255)',
        'StillInApi' => 'Boolean',
        'ProductPage' => 'Boolean'
    );

    private static $has_one = array(
        //'ComparisonAllInOnePage' => ComparisonAllInOnePage::class,
        'WlabelInternetSupplier' => WlabelInternetSupplier::class
    );

    public function MbDownloadSpeed(){

        $mb = round($this->DownloadSpeed / 1000);
        return floor($mb/5) * 5;

    }

    public function MbUploadSpeed(){

        $mb = round($this->UploadSpeed / 1000);
        $output = floor($mb/5) * 5;
        if ($mb < 10){
            $output = floor($mb/1) * 1;
        }
        if ($mb < 1){
            $output = $this->UploadSpeed/1000;
        }
        return $output;

    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return $output;

    }

    public function RatingAverageScore(){

        $output = round($this->RatingScore / $this->RatingVotes, 1);
        return number_format($output, 1, '.', '');

    }

    public function onBeforeWrite()
    {

        if ($this->ID && $this->isChanged('UrlId')){

            $old = WlabelInternetSubscription::get()->byID($this->ID);

            $add = new WlabelInternetOldUrl();
            $add->UrlId = $old->UrlId;
            $add->ShortUrlId = $old->ShortUrlId;
            $add->WlabelInternetSubscriptionID = $this->ID;
            $add->write();

            $this->ProductPage = 0;

        }

        parent::onBeforeWrite();

    }

}