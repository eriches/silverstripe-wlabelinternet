<?php

namespace Hestec\WlabelInternet;

use SilverStripe\ORM\DataObject;

class WlabelInternetOldUrl extends DataObject {

    private static $singular_name = 'WlabelInternetOldUrl';
    private static $plural_name = 'WlabelInternetOldUrls';

    private static $table_name = 'WlabelInternetOldUrl';

    private static $db = array(
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)'
    );

    private static $has_one = array(
        'WlabelInternetSubscription' => WlabelInternetSubscription::class
    );

}