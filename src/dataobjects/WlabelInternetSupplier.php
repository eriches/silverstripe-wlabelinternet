<?php

namespace Hestec\WlabelInternet;

use SilverStripe\ORM\DataObject;

class WlabelInternetSupplier extends DataObject {

    private static $singular_name = 'WlabelInternetSupplier';
    private static $plural_name = 'WlabelInternetSuppliers';

    private static $table_name = 'WlabelInternetSupplier';

    private static $db = array(
        'WlabelID' => 'Int',
        'SystemName' => 'Varchar(50)',
        'Name' => 'Varchar(50)'
    );

    /*private static $has_one = array(
        'SupplierPage' => SupplierPage::class
    );*/

    private static $has_many = array(
        'WlabelInternetSubscriptions' => WlabelInternetSubscription::class
    );

    /*private static $summary_fields = array(
        'Title',
        'StartDate.Nice',
        'EndDate.Nice',
        'Enabled.Nice'
    );*/

}