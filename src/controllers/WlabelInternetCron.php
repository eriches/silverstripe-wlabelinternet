<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use Hestec\WlabelInternet\WlabelInternetSubscription;
use Hestec\WlabelInternet\WlabelInternetSupplier;
use SilverStripe\Core\Config\Config;

class WlabelInternetCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateWlabelInternet',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv=true&include_phone=true&page=1&items_per_page=1', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateWlabelInternet() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $this->UpdateByCategory('all-in-one');
            $this->UpdateByCategory('internet-tv');
            $this->UpdateByCategory('internet-phone');
            $this->UpdateByCategory('internet');

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function UpdateByCategory($category) {

        switch($category){
            case "all-in-one":
                $include_tv = 'true';
                $include_phone = 'true';
                break;
            case "internet-tv":
                $include_tv = 'true';
                $include_phone = 'false';
                break;
            case "internet-phone":
                $include_tv = 'false';
                $include_phone = 'true';
                break;
            case "internet":
                $include_tv = 'false';
                $include_phone = 'false';
                break;
        }

        $whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv='.$include_tv.'&include_phone='.$include_phone.'&page=1&items_per_page=999', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);

        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Internet", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            DB::query("UPDATE WlabelInternetSubscription SET StillInApi = 0 WHERE Category = '".$category."'");

            foreach ($msgs->products as $node) {

                $count = WlabelInternetSubscription::get()->filter('BaseId', $node->base_id)->count();

                if ($count == 0){

                    $sub = new WlabelInternetSubscription();

                }else{

                    $sub = WlabelInternetSubscription::get()->filter('BaseId', $node->base_id)->first();

                }

                // if the base_name starts with the provider_name, remove it
                $basename = $this->removeProviderName($node->base_name, $node->provider_name);

                $sub->UrlId = SiteTree::create()->generateURLSegment($node->provider_name . "-" . $basename . "-" . $node->base_id);
                $sub->ShortUrlId = SiteTree::create()->generateURLSegment($node->base_id . "-" . $basename);
                $sub->StillInApi = 1; // see the DB::query above
                $sub->BaseId = $node->base_id;
                $sub->Category = $category;
                $sub->BaseName = $basename;
                $sub->Provider = $node->provider;
                $sub->ProviderId = $node->provider_id;
                $sub->ProviderName = $node->provider_name;
                $sub->BasePrice = $node->base_price;
                $sub->BasePriceInitial = $node->base_price_initial;
                $sub->DiscountPrice = $node->discount_price;
                $sub->DiscountAmount = $node->discount_amount;
                $sub->DiscountAmountInitial = $node->discount_amount_initial;
                $sub->DiscountAmountMonthly = $node->discount_amount_monthly;
                $sub->DiscountPriceInitial = $node->discount_price_initial;
                $sub->DiscountDuration = $node->discount_duration;
                $sub->DiscountType = $node->discount_type;
                $sub->TotalPriceInitial = $node->total_price_initial;
                $sub->TotalPrice = $node->total_price;
                $sub->TotalPriceDiscounted = $node->total_price_discounted;
                $sub->ComparePrice = $node->compare_price;
                $sub->ContractDuration = $node->contract_duration_text;
                $sub->ConnectionType = $node->internet->connection;
                $sub->DownloadSpeed = $node->internet->speed_down;
                $sub->UploadSpeed = $node->internet->speed_up;
                $sub->TvChannelsTotal = $node->tv->channel_count;
                $sub->TvChannelsHd = $node->tv->channel_count_hd;

                $sub->DiscountText = $node->discount_text;
                $sub->OfferText = $node->offer;

                $sub->ExtraIncluded = $node->additional->enabled;
                $sub->ExtraOptional = $node->additional->available;
                $sub->Score = $node->score_elements;

                $sub->SignupUrl = $node->signup_url;

                $votes = rand(0, 2);
                $score = rand(7, 10) * $votes;
                $sub->RatingVotes = $sub->RatingVotes + $votes;
                $sub->RatingScore = $sub->RatingScore + $score;

                if ($supplier = WlabelInternetSupplier::get()->filter('WlabelID', $node->provider_id)->first()) {

                    $sub->WlabelInternetSupplierID = $supplier->ID;

                }else{

                    $supplier = new WlabelInternetSupplier();
                    $supplier->WlabelID = $node->provider_id;
                    $supplier->SystemName = $node->provider;
                    $supplier->Name = $node->provider_name;
                    $supplier->write();

                    $sub->WlabelInternetSupplierID = $supplier->ID;

                    $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "New supplier added for Whitelabeled Internet", "The supplier ".$node->provider_name." is added for Whitelabeled Internet");
                    $email->sendPlain();

                }

                $sub->write();

            }
        }

    }

    public function removeProviderName($string, $startString){

        $len = strlen($startString);
        if (substr(strtolower($string), 0, $len) === strtolower($startString)){
            return substr($string, $len+1);
        }
        return $string;

    }

}
